# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=xdg-desktop-portal
pkgver=1.17.2
pkgrel=0
pkgdesc="Desktop integration portal"
url="https://github.com/flatpak/xdg-desktop-portal"
arch="all"
license="LGPL-2.1-or-later"
depends="bubblewrap cmd:fusermount3"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	flatpak
	flatpak-dev
	fontconfig-dev
	fuse3-dev
	geoclue-dev
	gettext-dev
	glib-dev
	json-glib-dev
	libportal-dev
	meson
	pipewire-dev
	py3-docutils
	xmlto
	"
checkdepends="py3-dbusmock"
subpackages="$pkgname-dev $pkgname-lang $pkgname-doc"
source="https://github.com/flatpak/xdg-desktop-portal/releases/download/$pkgver/xdg-desktop-portal-$pkgver.tar.xz"
# check: requires geoclue to be running as a system service.
options="!check"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dsystemd=disabled \
		-Dman-pages=enabled \
		-Ddocbook-docs=disabled \
		. output
	meson compile -C output
}

check() {
	TEST_IN_CI=true meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	cd "$pkgdir"

	# We don't need this
	rm -rf usr/lib/systemd

	mkdir -p usr/lib
	mv usr/share/pkgconfig usr/lib/
}

sha512sums="
2c54e9d615ef47345b1d6e09b3fb33618e0135c72297c27aeafaad23cf5e332483054f1817da800104271e3f4a575b6fd3961f72f84b07fe7a5c5f8b01903263  xdg-desktop-portal-1.17.2.tar.xz
"
