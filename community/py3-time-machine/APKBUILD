# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=py3-time-machine
pkgver=2.12.0
pkgrel=0
pkgdesc="Python library for mocking the current time"
url="https://github.com/adamchainz/time-machine"
arch="all"
license="MIT"
depends="py3-dateutil"
makedepends="
	python3-dev
	py3-setuptools
	py3-gpep517
	py3-wheel
	"
checkdepends="py3-pytest tzdata"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/adamchainz/time-machine/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/time-machine-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
4708cdc3aa0ec06907d0ee1219ceb074947fc962ab6f0d3606e5273f3d64ca278bb43254d924c04364b967301cf69229ced70d7b1db92b0b6dff1523cccb2f82  py3-time-machine-2.12.0.tar.gz
"
