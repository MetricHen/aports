# Contributor: Clayton Craft <clayton@craftyguy.net>
# Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=py3-lsp-server
pkgver=1.7.4
pkgrel=1
pkgdesc="python implementation of the language server protocol, fork of python-language-server"
url="https://github.com/python-lsp/python-lsp-server"
arch="noarch !armhf !s390x"  # armhf: no py3-qt, s390x: no py3-pylint
license="MIT"
depends="
	py3-docstring-to-markdown
	py3-jedi
	py3-lsp-jsonrpc
	py3-pluggy
	py3-setuptools
	py3-ujson
	python3
"
makedepends="
	py3-gpep517
	py3-setuptools_scm
	py3-wheel
"
checkdepends="
	py3-autopep8
	py3-flake8
	py3-flaky
	py3-matplotlib
	py3-mccabe
	py3-numpy
	py3-pandas
	py3-pycodestyle
	py3-pydocstyle
	py3-pyflakes
	py3-pylint
	py3-pytest
	py3-pytest-cov
	py3-python-versioneer
	py3-qt5
	py3-rope
	py3-toml
	py3-whatthepatch
	py3-yapf
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/p/python-lsp-server/python-lsp-server-$pkgver.tar.gz
	416.patch"
builddir="$srcdir/python-lsp-server-$pkgver"
# check: fails with 416.patch
options="!check"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# deselect'ed tests are broken
	.testenv/bin/python3 -m pytest \
		--deselect test/plugins/test_pydocstyle_lint.py \
		-k 'not test_jedi_completion_environment and not test_symbols_all_scopes_with_jedi_environment'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
7aa96de6e51548e2854b55690698fe6c7bda60103c11ad5982df611c65569cec1ab13691727c92ec47d9ac44c34f233f872211a41041ea0969223e10e87d7e08  python-lsp-server-1.7.4.tar.gz
042cbd8793ef4324427d15b8904e9e485d8ad9aa9023d3026ac156fc139d7215fa6b42c3521e6766bd0ded93d48e31c8675ddbd708c3d5ace16e5aaf1a41cf39  416.patch
"
