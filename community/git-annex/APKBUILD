# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=git-annex
pkgver=10.20230828
pkgrel=0
pkgdesc="Manage files with git, without checking their contents into git"
url="http://git-annex.branchable.com"
arch="x86_64 aarch64" # limited by ghc
license="AGPL-3.0-or-later"
options="net"
depends="
	curl
	git
	rsync
	"
makedepends="
	alex
	cabal
	dbus-dev
	file-dev
	ghc
	gmp-dev
	gnutls-dev
	happy
	libffi-dev
	libgsasl-dev
	libxml2-dev
	ncurses-dev
	zlib-dev
	"
source="
	https://git.joeyh.name/index.cgi/git-annex.git/snapshot/git-annex-$pkgver.tar.gz
	git-annex.config
	git-annex-block-crypton.patch
	fix-makefile.patch
	"
subpackages="$pkgname-doc $pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion"

# Add / remove '-' between "-f" and "FeatureName" to adjust feature build
_feature_flags="
	-fAssistant \
	-fWebApp \
	-fPairing \
	-fProduction \
	-fTorrentParser \
	-fMagicMime \
	-fBenchmark \
	-f-DebugLocks \
	-fDbus \
	-fNetworkBSD \
	-fGitLfs \
	-fHttpClientRestricted \
	"
_cabal_home="$srcdir/dist"

cabal_update() {
	default_prepare
	msg "Freezing $pkgname dependencies"

	# Resolve deps and generate fresh cabal.config with version constraints.
	HOME="$_cabal_home" cabal update
	cd "$srcdir/git-annex"*
	HOME="$_cabal_home" cabal v1-freeze --shadow-installed-packages

	# Add version tag at the first line.
	sed -i "1i--$pkgver" "cabal.config"

	mv "cabal.config" "$startdir/git-annex.config"
	cd "$startdir"

	if ! abuild checksum; then
		die "Failed to update checksum, run 'abuild checksum' manually"
	fi
}

prepare() {
	default_prepare

	if [ "$(head -n 1 "$srcdir/git-annex.config")" != "--$pkgver" ]; then
		die "Requirements file is outdated, run 'abuild cabal_update'"
	fi

	ln -sf "$srcdir/git-annex.config" cabal.config

	# ghc version path
	export PATH="$PATH:/usr/lib/llvm14/bin"

	# problematic depend install
	HOME="$_cabal_home" cabal update
}

build() {
	# ghc version path
	export PATH="$PATH:/usr/lib/llvm14/bin"

	msg "Building git-annex-$pkgver"
	HOME="$_cabal_home" cabal update
	HOME="$_cabal_home" cabal v1-install \
		--only-dependencies \
		--allow-newer=feed:base-compat \
		$_feature_flags
	HOME="$_cabal_home" cabal v1-configure $_feature_flags
	HOME="$_cabal_home" cabal v1-build -j
	mv dist/build/git-annex/git-annex .
	ln -s git-annex git-annex-shell
}

check() {
	"$builddir"/git-annex test
}

package() {
	HOME="$_cabal_home" make DESTDIR="$pkgdir" install
}

sha512sums="
a25e57f0cb632934c521ab34dd2b71bf3c98de768b817cb6fc4af76fb00ea281bb0f1e9a33f347ed69ddded71018562ac8061debd7189d12d067408cec5282b5  git-annex-10.20230828.tar.gz
f5d535f9012dd3a67d61b8404883154cfebeb0d36a98f63c65896326b6a3893678c81926535afa0e862498a7ec11ee0b65be5e004aa93506b87d74995f4f9ae1  git-annex.config
700e561b79e186839dfb8c63b185ce5f83745f8c43473e0fd99df3cdae2165073a8fe4b1cfb2798b3ad852e22e3eea84a05b884ef10b89f80f9881e0921aab2f  git-annex-block-crypton.patch
9300f883746d8726f47be6d194b5ac9550e2894669097f3202eca944221665bd3087a81b3f97b21f013eccaa6b5b1fb050e253ac18999c136db20056fadf2ed8  fix-makefile.patch
"
