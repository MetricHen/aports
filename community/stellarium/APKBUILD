# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer:
pkgname=stellarium
pkgver=23.2
pkgrel=0
pkgdesc="A stellarium with great graphics and a nice database of sky-objects"
url="https://stellarium.org/"
# gigantic package
arch="aarch64 x86_64"
license="GPL-2.0-or-later"
makedepends="
	boost-dev
	cmake
	freetype-dev
	gpsd-dev
	libpng-dev
	mesa-dev
	openssl-dev>3
	qt5-qtcharts-dev
	qt5-qtlocation-dev
	qt5-qtmultimedia-dev
	qt5-qtscript-dev
	qt5-qtserialport-dev
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-doc"
source="https://github.com/Stellarium/stellarium/releases/download/v$pkgver/stellarium-$pkgver.tar.xz"

build() {
	# SHOWMYSKY support needs qt5-qtopengl
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_TESTING=1 \
		-DENABLE_SHOWMYSKY=OFF
	cmake --build build
}

check() {
	cd build
	# Exclude a broken locale test
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'test(INDIConnection|TelescopeControl_INDI|TelescopeClientINDI)'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ba1ab70367eb626fdc831a5845a25d5deb6ed4d89c1bd6acc9b769e1cd739afb6bc98263a842e3153e1015796e5ef132fd1077b590010c4de1090e13928a7200  stellarium-23.2.tar.xz
"
