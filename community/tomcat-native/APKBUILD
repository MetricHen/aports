# Contributor: Sean Summers <seansummers@gmail.com>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=tomcat-native
pkgver=2.0.5
pkgrel=0
pkgdesc="Native resources optional component for Apache Tomcat"
url="https://tomcat.apache.org/native-doc/"
# riscv64: blocked by java-jdk
arch="all !riscv64"
license="Apache-2.0"
makedepends="
	apr-dev
	chrpath
	java-jdk
	libtool
	openssl-dev
	"
subpackages="$pkgname-dev"
source="https://archive.apache.org/dist/tomcat/tomcat-connectors/native/$pkgver/source/tomcat-native-$pkgver-src.tar.gz"
builddir="$srcdir/$pkgname-$pkgver-src/native"
options="!check" # package has no tests

build() {
	./configure \
		--prefix=/usr \
		--with-apr=/usr/bin/apr-1-config \
		--with-java-home=/usr/lib/jvm/default-jvm \
		--with-ssl=/usr/lib
	make
}

package() {
	make DESTDIR="$pkgdir" install

	# Remove redundant rpath.
	chrpath --delete "$pkgdir"/usr/lib/libtcnative-*.so

	rm -f "$pkgdir"/usr/lib/*.la
}

dev() {
	default_dev
	mv "$subpkgdir"/usr/lib/libtcnative-*.so "$pkgdir"/usr/lib/
}

sha512sums="
03ed1951597bb6a6ade8d715546a0d635d261381737abd27b63aa552c69f9ca49405d0252b6bc2d878bfd64e87439f146ecd84ccbfc82661ba16379c493615a4  tomcat-native-2.0.5-src.tar.gz
"
