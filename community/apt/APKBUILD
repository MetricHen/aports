# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=apt
pkgver=2.7.5
pkgrel=0
pkgdesc="APT package management tool"
url="https://salsa.debian.org/apt-team/apt"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	bzip2-dev
	db-dev
	cmake
	dpkg-dev
	eudev-dev
	gettext-dev
	gnutls-dev
	libgcrypt-dev
	lz4-dev
	samurai
	triehash
	xxhash-dev
	xz-dev
	zlib-dev
	zstd-dev
	"
checkdepends="gtest-dev"
subpackages="
	$pkgname-dev
	$pkgname-libs
	"
source="https://salsa.debian.org/apt-team/apt/-/archive/$pkgver/apt-$pkgver.tar.bz2"
options="!check" # todo

build() {
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DWITH_DOC=OFF \
		-DUSE_NLS=ON \
		-DWITH_TESTS="$(want_check && echo ON || echo OFF)"
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# libraries only
	cd "$pkgdir"
	rm -r usr/bin usr/libexec usr/share var etc
}

sha512sums="
9ac79273cdddb4e4c93e3da011d81e3742e89770a39e452625f4fac31453d93a1332e473951b1604200996676b8300cca57ad64e48a4e42b31d774a336e22fb3  apt-2.7.5.tar.bz2
"
