# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=py3-plexapi
_pkgname=python-plexapi
pkgver=4.15.2
pkgrel=0
pkgdesc="Python bindings for the Plex API"
url="https://github.com/pkkid/python-plexapi"
arch="noarch"
license="BSD-3-Clause"
# tests requires an instance of plex running
# net for sphinx
options="net !check"
depends="
	python3
	py3-requests
	"
makedepends="
	py3-gpep517
	py3-recommonmark
	py3-setuptools
	py3-sphinx_rtd_theme
	py3-wheel
	"
subpackages="$pkgname-doc $pkgname-pyc"
source="https://github.com/pkkid/python-plexapi/archive/$pkgver/py3-plexapi-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
	sphinx-build -W -b man docs man
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
	install -Dm644 man/pythonplexapi.1 -t "$pkgdir"/usr/share/man/man1
}

sha512sums="
220fd75b6b109c25752e1d2fc16aa469cab054919346e1982f8e4229d84a103aa3c70a3503c0e85e8a4002ab5df528dc7e4942ae0876dad5c898171a17f227f0  py3-plexapi-4.15.2.tar.gz
"
