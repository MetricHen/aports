# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-mitmproxy-rs
pkgver=0.3.2
pkgrel=0
pkgdesc="mitmproxy modules written in Rust"
url="https://github.com/mitmproxy/mitmproxy_rs"
license="MIT"
arch="all !ppc64le !riscv64 !s390x" # fails to build ring crate
makedepends="
	cargo
	protoc
	py3-gpep517
	py3-installer
	py3-maturin
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://github.com/mitmproxy/mitmproxy_rs/archive/$pkgver/py3-mitmproxy-rs-$pkgver.tar.gz
	dont-use-vendored-protoc.patch
	"
builddir="$srcdir/mitmproxy_rs-$pkgver"
options="net" # cargo

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cd mitmproxy-rs
	gpep517 build-wheel \
		--wheel-dir "$builddir"/.dist \
		--output-fd 3 3>&1 >&2
}

check() {
	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
7701731c4747cc0d4a77a5257a82d64109a63d350e7c8b98570d5f4ef74f419d9301cb713693b2eef266ea4ef29e97d258dbe54283739bed53e5f9dcfb627f38  py3-mitmproxy-rs-0.3.2.tar.gz
d8f13164ad23e4e4385c11069b6feeb37a768a3cd302c086f63d1960dce72b4ea2c2c11a7938399af67a8ae5dcc183c25e48f8eb72ed06699afb555366361fb5  dont-use-vendored-protoc.patch
"
